import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import javax.swing.*;
@SpringBootApplication
@ComponentScan("com.mock.dongyang")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class,args);
    }
}
