package com.mock.dongyang;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.spring.web.json.Json;

import javax.servlet.http.HttpServletResponse;


@Api(value = "/")
@RestController
public class MockTest {

    @ApiOperation(value = "mock首页数据统计接口",httpMethod = "get")
    @RequestMapping(value = "v1/government/index/count",method = RequestMethod.GET)
    public Json getIndex(HttpServletResponse response,
                         @RequestParam(value = "_t",required = true) String _t,
                         @RequestParam(value = "platform",required = true) String platform){
        if(platform.equals("h5")){
            System.out.println("mock 数据成功！！");
        }

        response.setHeader("Content-Type","application/json");
//        response.setHeader("Server","nginx/1.15.11");
//        response.setHeader("Transfer-Encoding","chunked");
//        response.setHeader("Cache-Control","no-cache,private");
//        response.setHeader("Access-Control-Allow-Credentials","true");
//        response.setHeader("Access-Control-Allow-Headers","X-Requested-With, Origin, X-Csrftoken, Content-Type, Accept");
//        response.setHeader("Access-Control-Allow-Methods","GET, PUT, POST, DELETE, HEAD, OPTIONS");
        response.setHeader("Access-Control-Allow-Origin","*");
//        response.setHeader("X-RateLimit-Limit","60000");
//        response.setHeader("X-RateLimit-Remaining","59979");

        String s = null;
        s = "{\n" +
                "        \"code\": \"200\",\n" +
                "        \"message\": \"success\",\n" +
                "        \"data\": {\n" +
                "          \"import_org_num\": 330,\n" +
                "          \"add_org_num\": 1140,\n" +
                "          \"org_help_num\": 670,\n" +
                "          \"supply_0_num\": 760,\n" +
                "          \"supply_1_num\": 380,\n" +
                "          \"policy_num\": 270,\n" +
                "          \"bank_product_num\": 150,\n" +
                "          \"bank_demand_num\": 360\n" +
                "        }\n" +
                "      }";
        Json jsonDate = new Json(s);
        return jsonDate;
    }

    @ApiOperation(value = "mock诉求列表接口",httpMethod = "get")
    @RequestMapping(value = "v1/government/help/index",method = RequestMethod.GET)
    public Json getHelpIndex(HttpServletResponse response,
                         @RequestParam(value = "_t",required = true) String _t,
                         @RequestParam(value = "platform",required = true) String platform){
        if(platform.equals("h5")){
            System.out.println("mock 数据成功！！");
        }

        response.setHeader("Content-Type","application/json");
//        response.setHeader("Server","nginx/1.15.11");
//        response.setHeader("Transfer-Encoding","chunked");
//        response.setHeader("Cache-Control","no-cache,private");
//        response.setHeader("Access-Control-Allow-Credentials","true");
//        response.setHeader("Access-Control-Allow-Headers","X-Requested-With, Origin, X-Csrftoken, Content-Type, Accept");
//        response.setHeader("Access-Control-Allow-Methods","GET, PUT, POST, DELETE, HEAD, OPTIONS");
        response.setHeader("Access-Control-Allow-Origin","*");
//        response.setHeader("X-RateLimit-Limit","60000");
//        response.setHeader("X-RateLimit-Remaining","59979");

        String s = null;
        String st = null;
        st = "{\n" +
                "\t\t\t\torganization_help_id: 166,\n" +
                "\t\t\t\tname: \"防守打法是否水电费\",\n" +
                "\t\t\t\torganization_id: 127,\n" +
                "\t\t\t\thelp_type: 2,\n" +
                "\t\t\t\tlinker_tel: \"13777870244\",\n" +
                "\t\t\t\tcontent_p: \"反倒是11\",\n" +
                "\t\t\t\turgent: 1,\n" +
                "\t\t\t\tstatus: 0,\n" +
                "\t\t\t\tcreated_at: \"2020-04-19 16:50:49\",\n" +
                "\t\t\t\tvalid_time: null,\n" +
                "\t\t\t\tarea: 11,\n" +
                "\t\t\t\thelp_type_text: \"资金问题\",\n" +
                "\t\t\t\tcreater: {\n" +
                "\t\t\t\t\tnickname: \"用户1242\",\n" +
                "\t\t\t\t\tavatar: \"//img2019.jiheapp.com/img/20200416/eb16888bf02a2b815c2e0daccc6f2466.jpg\"\n" +
                "\t\t\t\t}\n" +
                "\t\t\t}";

        for(int i=1;i<10;i++){
            st = st + ',' + st;
        }

        s = "{\n" +
                "\tcode: \"200\",\n" +
                "\tmessage: \"操作成功\",\n" +
                "\tdata: {\n" +
                "\t\tcurrent_page: 1,\n" +
                "\t\tdata: [\n" +
                "\t\t\t{\n" +
                "\t\t\t\torganization_help_id: 167,\n" +
                "\t\t\t\tname: \"湖西镇大企业\",\n" +
                "\t\t\t\torganization_id: 141,\n" +
                "\t\t\t\thelp_type: 1,\n" +
                "\t\t\t\tlinker_tel: \"13777870244\",\n" +
                "\t\t\t\tcontent_p: \"我先选新西湖大企业\",\n" +
                "\t\t\t\turgent: 1,\n" +
                "\t\t\t\tstatus: 1,\n" +
                "\t\t\t\tcreated_at: \"2020-04-20 09:56:52\",\n" +
                "\t\t\t\tvalid_time: null,\n" +
                "\t\t\t\tarea: 13,\n" +
                "\t\t\t\thelp_type_text: \"土地问题\",\n" +
                "\t\t\t\tcreater: {\n" +
                "\t\t\t\tnickname: \"用户1242\",\n" +
                "\t\t\t\tavatar: \"//img2019.jiheapp.com/img/20200416/eb16888bf02a2b815c2e0daccc6f2466.jpg\"\n" +
                "\t\t\t}\n" +
                "\t\t\t,\n" +
                "\t\t\t{\n" +
                "\t\t\t\torganization_help_id: 166,\n" +
                "\t\t\t\tname: \"防守打法是否水电费\",\n" +
                "\t\t\t\torganization_id: 127,\n" +
                "\t\t\t\thelp_type: 2,\n" +
                "\t\t\t\tlinker_tel: \"13777870244\",\n" +
                "\t\t\t\tcontent_p: \"反倒是11\",\n" +
                "\t\t\t\turgent: 1,\n" +
                "\t\t\t\tstatus: 0,\n" +
                "\t\t\t\tcreated_at: \"2020-04-19 16:50:49\",\n" +
                "\t\t\t\tvalid_time: null,\n" +
                "\t\t\t\tarea: 11,\n" +
                "\t\t\t\thelp_type_text: \"资金问题\",\n" +
                "\t\t\t\tcreater: {\n" +
                "\t\t\t\t\tnickname: \"用户1242\",\n" +
                "\t\t\t\t\tavatar: \"//img2019.jiheapp.com/img/20200416/eb16888bf02a2b815c2e0daccc6f2466.jpg\"\n" +
                "\t\t\t\t}\n" +
                "\t\t\t}\n" +","+ st +
                "\t\t],\n" +
                "\t\tfirst_page_url: \"http://183.129.234.53:8999/v1/government/help/index?page=1\",\n" +
                "\t\tfrom: 1,\n" +
                "\t\tlast_page: 8,\n" +
                "\t\tlast_page_url: \"http://183.129.234.53:8999/v1/government/help/index?page=8\",\n" +
                "\t\tnext_page_url: \"http://183.129.234.53:8999/v1/government/help/index?page=2\",\n" +
                "\t\tpath: \"http://183.129.234.53:8999/v1/government/help/index\",\n" +
                "\t\tper_page: 10,\n" +
                "\t\tprev_page_url: null,\n" +
                "\t\tto: 10,\n" +
                "\t\ttotal: 71\n" +
                "\t\t}\n" +
                "}";



        Json jsonDate = new Json(s);
        return jsonDate;
    }
}
