package com.course.server;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Api(value = "/",description = "所有get接口")
@RestController
public class MyGetMethod {
    /**
    * 返回cookies信息接口
    * */
    @ApiOperation(value = "获取cookies的接口",httpMethod = "GET")
    @RequestMapping(value = "/getCookies",method = RequestMethod.GET)
    public String getCookies(HttpServletResponse response){
        //HttpServerLetRequest  装请求信息类
        //HttpServerLetResponse 装响应信息类
        Cookie cookie1 = new Cookie("cook","thiscookies");
        Cookie cookie2 = new Cookie("status","110");
        response.addCookie(cookie1);
        response.addCookie(cookie2);
        return "获取cookies信息成功！！";
    }

    /**
     * 这个接口需要用到cookie信息
     * */
    @ApiOperation(value = "需要携带cookies才能访问的接口",httpMethod = "GET")
    @RequestMapping(value = "/useCookies",method = RequestMethod.GET)
    public String useCookies(HttpServletRequest request){
        Cookie[] cookies = request.getCookies();
        if(Objects.isNull(cookies)){
            return "必须携带cookies信息！";
        }
        for(Cookie cookie:cookies){
            System.out.println(cookie.getName());
            if(cookie.getName().equals("cook")&&cookie.getValue().equals("thiscookies")) {
                return "验证cookies信息1通过";
            }
            if (cookie.getName().equals("status")&&cookie.getValue().equals("110")){
                return "恭喜利用cookie信息访问成功";
            }
        }
        return "该接口需要cookies信息！";
    }

    /***
     * 开发一个带有参数的接口
     *
     */
    @ApiOperation(value = "需要传入两个参数才能访问的接口",httpMethod = "GET")
    @RequestMapping(value = "/useParamGet",method = RequestMethod.GET)
    public Map<String,Integer> getList(@RequestParam Integer start,
                                       @RequestParam Integer end){
        Map<String,Integer> myList = new HashMap<>();
        myList.put("sheep",200);
        myList.put("peg",100);
        myList.put("cow",300);
        return myList;
    }

    /***
     * 开发一个带有参数的纯路径请求
     */
    @ApiOperation(value = "需要两个参数拼接在路径中才能访问",httpMethod = "GET")
    @RequestMapping(value = "/useParamGet/{start}/{end}")
    public Map getListAnother(@PathVariable Integer start,
                              @PathVariable Integer end){
        Map<String,Integer> myList = new HashMap<>();
        myList.put("sheep",200);
        myList.put("peg",100);
        myList.put("cow",300);
        return myList;
    }

}
