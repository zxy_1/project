package com.course.server;

import com.course.bean.User;
import com.course.bean.UserParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

@Api(value = "/",description = "所有的post接口")
@RestController
@RequestMapping(value = "/v1")
public class MyPostMethod {
    //cookies信息
    private Cookie cookie;

    @ApiOperation(value = "登陆并获取cookie接口",httpMethod = "POST")
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public String login(HttpServletResponse response,
                        @RequestParam(value = "userName",required = true) String userName,
                        @RequestParam(value = "password",required = true) String password){
        if(userName.equals("zhangsan")&&password.equals("123456")){
            cookie = new Cookie("cook","thiscookies");
            response.addCookie(cookie);
            return "登陆成功欢迎您！！！";
        }
        return "登陆失败，请检查您的用户名密码是否正确！！！";
    }

    @ApiOperation(value = "带参数并要求cookies信息，返回用户信息的post接口",httpMethod = "POST")
    @RequestMapping(value = "/getUserInfor",method = RequestMethod.POST)
    public String getUserInfor(HttpServletRequest request,
                               @RequestBody UserParam userParam){
        Cookie[] cookies = request.getCookies();
        if(Objects.isNull(cookies)){
            return "cookies信息已过期！！";
        }
        for(Cookie cookie:cookies){
            if(cookie.getName().equals("cook")
                    &&cookie.getValue().equals("thiscookies")
                    &&userParam.getToken().equals("thistoken")
                    &&userParam.getUserid().equals("110")){

                User user = new User();
                user.setUserName("zhangsan");
                user.setPassword("123456");
                user.setAge("18");
                user.setSex("男");
                return user.toString();
            }
        }
        return "参数错误！！";
    }
}
