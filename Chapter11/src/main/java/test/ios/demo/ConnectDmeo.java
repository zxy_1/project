package test.ios.demo;

import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;

public class ConnectDmeo {
    public static void main(String[]args) throws Exception {
        String appAddress = "/Users/zhuxingyue/Library/Developer/Xcode/DerivedData/WebDriverAgent-afconskmwmsypggtuwpbuzsblfiv/Build/Products/Debug-iphonesimulator/IntegrationApp.app";
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("platformName","ios");
        capabilities.setCapability("platformVersion","13.5");
        capabilities.setCapability("app",appAddress);
        capabilities.setCapability("deviceName","iphone 8");
        capabilities.setCapability("udid","D8F4EE34-A199-479C-A5E2-3669BD19F750");
        capabilities.setCapability("automationName", "XCUITest");
        capabilities.setCapability("noReset","true");

        URL url = new URL("http://localhost:4723/wd/hub");
        IOSDriver<IOSElement> driver = new IOSDriver(url,capabilities);

//        driver.findElement(By.name("Alerts")).click();
        driver.findElement(By.name("Scrolling")).click();
        driver.findElement(By.name("TableView")).click();
        String value = driver.findElement(By.id("2")).getText();
        System.out.println(value);
        driver.findElement(By.xpath("//XCUIElementTypeTable[@name=\"scrollView\"]")).click();



        Thread.sleep(3000);
        driver.quit();
    }

}
