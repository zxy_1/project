package com.app.baseModule;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * 连接设备配置
 */

public class DriverGet {
    private AndroidDriver<WebElement> driver = null;
    private final DesiredCapabilities apabilities;

    /**
     * 初始连接设备创建driver对象
     */
    public DriverGet(){
        apabilities = new DesiredCapabilities();
        apabilities.setCapability("automationName", "Appium");
        apabilities.setCapability("platformName", "Android");
        apabilities.setCapability("deviceName", "1e9a9bd2");
        apabilities.setCapability("platformVersion", "8.0");
        apabilities.setCapability("appPackage", "com.guanghua.jiheuniversity");
        apabilities.setCapability("appActivity", "com.guanghua.jiheuniversity.MainActivity");

        // A new session could not be created的解决方法
        //apabilities.setCapability("appWaitActivity", "com.cunshuapp.cunshu.vp.user.login.UserLoginActivity");
//        apabilities.setCapability("appWaitActivity", "com.guanghua.jiheuniversity.MainActivity");
        // 每次启动时覆盖session，否则第二次后运行会报错不能新建session
        apabilities.setCapability("sessionOverride", true);

        try {
            driver = new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), apabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取driver对象
     * @return 返回driver
     */
    public AndroidDriver<WebElement> getDriver() {
        return this.driver;
    }
}
