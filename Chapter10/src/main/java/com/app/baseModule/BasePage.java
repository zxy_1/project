package com.app.baseModule;
/**
* 基础页面
* */
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class BasePage {
    protected AndroidDriver driver;

    public BasePage(AndroidDriver driver){
        this.driver = driver;
    }

    /**
     * 获取元素
     * @param by 定位信息
     * @return  返回元素对象
     */
    public Object findElement(By by){
        return driver.findElement(by);
    }

    /**
     * 点击方法
     * @param element 传入元素对象
     */
    public void click(WebElement element){
       element.click();
    }

    /**
     * 输入方法
     * @param element 传入的元素对象
     * @param key 要输入的值
     */
    public void input(WebElement element,String key){
        element.sendKeys(key);
    }

    /**
     * 获取元素的文字
     * @param element 元素对象
     * @return 元素的文字
     */
    public String getText(WebElement element){
        return element.getText();
    }
}
