package com.app.module;

import com.app.baseModule.BasePage;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * 搜索结果页面
 */

public class SearchResultActivtiy extends BasePage {
    //课程标签
    private final String searchResultId = "com.guanghua.jiheuniversity:id/item_text";

    public SearchResultActivtiy(AndroidDriver driver) {
        super(driver);
    }

    public String getSearchResult(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement element = (WebElement) findElement(By.id(searchResultId));

        return getText(element);
    }
}
