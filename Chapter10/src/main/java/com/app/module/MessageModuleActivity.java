package com.app.module;

import com.app.baseModule.BasePage;
import io.appium.java_client.android.AndroidDriver;

/**
 * 集合大学消息通知页面
 */

public class MessageModuleActivity extends BasePage {
    private final String titleId = "com.guanghua.jiheuniversity:id/tvCenterTitle";
    private final String backId = "com.guanghua.jiheuniversity:id/ivLeftBack";
    private final String allReadId = "com.guanghua.jiheuniversity:id/tvRightComplete";

    public MessageModuleActivity(AndroidDriver driver) {
        super(driver);
    }

    public MainActivity backClick(){
        return new MainActivity(driver);
    }

}
