package com.app.module;

import com.app.baseModule.BasePage;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *启动app获取权限弹窗
 */
public class AndroidPermissionActivtiy2 extends BasePage {

    //确定按钮id
    private final String allowId = "android:id/button1";

    //构造函数
    public AndroidPermissionActivtiy2(AndroidDriver driver) {
        super(driver);
    }

    /**
     * 获取权限确定按钮点击动作
     * @return 返回首页对象
     */
    public MainActivity allowClick(){
        WebElement webElement = (WebElement) findElement(By.id(allowId));


        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        click(webElement);

        return new MainActivity(driver);
    }
}
