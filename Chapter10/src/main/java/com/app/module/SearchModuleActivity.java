package com.app.module;

import com.app.baseModule.BasePage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * 集盒大学搜索页面
 */
public class SearchModuleActivity extends BasePage {
    //搜索输入框id
    private final String searchInputId = "com.guanghua.jiheuniversity:id/edit_search";
    //搜索按钮系统按键
    private final AndroidKey searchButtonPorztion = AndroidKey.SEARCH;

    public SearchModuleActivity(AndroidDriver driver) {
        super(driver);
    }

    /**
     * 搜索输入关键字
     * @param key 搜索关键字
     */
    public void searchKey(String key){
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement element = (WebElement) findElement(By.id(searchInputId));
        input(element,key);
    }

    /**
     * 点击搜索动作
     * @return 返回搜索结果页面
     */
    public SearchResultActivtiy searchButtonClick(){
        driver.pressKey(new KeyEvent(searchButtonPorztion));
        return new SearchResultActivtiy(driver);
    }

}
