package com.app.module;

import com.app.baseModule.BasePage;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * 集盒大学首页
 */

public class MainActivity extends BasePage {
    //日历按钮id
    private final String dateId = "com.guanghua.jiheuniversity:id/calendar";
    //搜索输入框id
    private final String searchId = "com.guanghua.jiheuniversity:id/tv_search";
    //消息通知按钮id
    private final String messageId = "com.guanghua.jiheuniversity:id/message_layout";
    //banner图片id
    private final String bannerId = "com.guanghua.jiheuniversity:id/banner_image";
    //学院图片id，所有学院使用同一个id
    private final String collegeId = "com.guanghua.jiheuniversity:id/name";//学院icon多个id相同（第一个：45）
    //限时活动图片id
    private final String activeId = "com.guanghua.jiheuniversity:id/limit_activities";
    //限时活动关闭按钮id
    private final String activeCloseId = "com.guanghua.jiheuniversity:id/close_activity";
    //免费课程id
    private final String freeCourseId = "com.guanghua.jiheuniversity:id/course_image";//免费课程多个id相同（第一个：43）

    public MainActivity(AndroidDriver driver) {
        super(driver);
    }

    /**
     * 点击搜索输入框
     * @return 返回搜索页面
     */
    public SearchModuleActivity searchClick(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement webElement = (WebElement) findElement(By.id(searchId));


        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        click(webElement);

        return new SearchModuleActivity(driver);
    }

    /**
     * 点击日历按钮
     */
    public void dataClick(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement webElement = (WebElement) findElement(By.id(dateId));


        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        click(webElement);

    }
}
