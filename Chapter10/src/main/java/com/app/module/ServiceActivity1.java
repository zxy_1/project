package com.app.module;

import com.app.baseModule.BasePage;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ServiceActivity1 extends BasePage {
    private final String yesId = "com.guanghua.jiheuniversity:id/tv_confirm";

    public ServiceActivity1(AndroidDriver driver) {
        super(driver);
    }

    public AndroidPermissionActivtiy2 yesClick(){
        WebElement webElement = (WebElement) findElement(By.id(yesId));


        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        click(webElement);

        return new AndroidPermissionActivtiy2(driver);
    }
}
