package com.app.testdemo;

import com.app.baseModule.DriverGet;
import com.app.module.MainActivity;
import com.app.module.SearchModuleActivity;
import com.app.module.SearchResultActivtiy;
import io.appium.java_client.android.AndroidDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SearchModuleActivityTestDemo {
    AndroidDriver driver = new DriverGet().getDriver();
    MainActivity mainActivity = null;
    SearchModuleActivity searchModuleActivity = null;
    SearchResultActivtiy searchResultActivtiy = null;

    @BeforeTest
    public void beforeTest(){
        mainActivity = new MainActivity(driver);
    }

    @Test
    public void testSearchClick() throws InterruptedException {
        Thread.sleep(10000);
//        if(mainActivity == null)
//            System.out.println("++++++++++++");
        searchModuleActivity = mainActivity.searchClick();
        searchModuleActivity.searchKey("测试");
        searchResultActivtiy = searchModuleActivity.searchButtonClick();
        String s = searchResultActivtiy.getSearchResult();
        System.out.println(s);
    }

}
