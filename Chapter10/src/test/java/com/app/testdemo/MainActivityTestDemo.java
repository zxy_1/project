package com.app.testdemo;


import com.app.module.AndroidPermissionActivtiy2;
import com.app.module.MainActivity;
import com.app.module.ServiceActivity1;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;


public class MainActivityTestDemo {
    AndroidDriver<WebElement> driver = null;
    DesiredCapabilities apabilities = null;
    ServiceActivity1 serviceActivity = null;
    AndroidPermissionActivtiy2 androidPermissionActivtiy2 = null;
    MainActivity mainActivity = null;

    @BeforeTest
    public void beforeTest(){
        apabilities = new DesiredCapabilities();
        apabilities.setCapability("automationName", "Appium");
        apabilities.setCapability("platformName", "Android");
        apabilities.setCapability("deviceName", "1e9a9bd2");
        apabilities.setCapability("platformVersion", "8.0");
        apabilities.setCapability("appPackage", "com.guanghua.jiheuniversity");
        apabilities.setCapability("appActivity", "com.guanghua.jiheuniversity.MainActivity");

        // A new session could not be created的解决方法
        //apabilities.setCapability("appWaitActivity", "com.cunshuapp.cunshu.vp.user.login.UserLoginActivity");
//        apabilities.setCapability("appWaitActivity", "com.guanghua.jiheuniversity.MainActivity");
        // 每次启动时覆盖session，否则第二次后运行会报错不能新建session
        apabilities.setCapability("sessionOverride", true);

        try {
            driver = new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), apabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testYesClick(){
        serviceActivity = new ServiceActivity1(driver) ;
        androidPermissionActivtiy2 = serviceActivity.yesClick();
        System.out.println("************");
    }

    @Test(dependsOnMethods = {"testYesClick"})
    public void testAllowClick(){
        mainActivity = androidPermissionActivtiy2.allowClick();
        System.out.println("---------------");
    }

    @Test
    public void testSearchClick(){
        if(mainActivity == null)
            mainActivity = new MainActivity(driver);
        mainActivity.searchClick();
        System.out.println("===============");
    }



}
