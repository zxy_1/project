package com.course.controller;

import com.course.model.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Log4j
@Api(value = "v1",description = "从数据库获取数据接口")
@RestController
@RequestMapping(value = "v1")
public class GetUserCount {

    @Autowired
    private SqlSessionTemplate template;

    @ApiOperation(value = "获取数据表中的数据条数",httpMethod = "GET")
    @RequestMapping(value = "/getUserCount",method = RequestMethod.GET)
    public String getUserCount(){
        String count = "这时接口查询数据库的数据条数：" + template.selectOne("getUseCount");
        return count;
    }

    @ApiOperation(value = "获取用信息",httpMethod = "GET")
    @RequestMapping(value = "/getUserInfor",method = RequestMethod.GET)
    public List<User> getUserInfor(@RequestParam int id){
        return  template.selectList("getUseInfor", id);
    }

    @ApiOperation(value = "数据库添加数据接口",httpMethod = "POST")
    @RequestMapping(value = "/addUser",method = RequestMethod.POST)
    public int addUser(@RequestBody User user){
        int result = template.insert("addUser",user);
        return result;
    }

    @ApiOperation(value = "数据库更新数据接口",httpMethod = "POST")
    @RequestMapping(value = "/updateUser",method = RequestMethod.POST)
    public int updateUser(@RequestBody User user){
        return template.update("updateUser",user);
    }

    @ApiOperation(value = "数据库删除数据",httpMethod = "POST")
    @RequestMapping(value = "/deleteUser",method = RequestMethod.POST)
    public int deleteUser(@RequestParam int id){
        int deleteUser = template.delete("deleteUser", id);
        return deleteUser;
    }
}
