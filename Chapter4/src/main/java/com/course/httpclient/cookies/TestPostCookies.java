package com.course.httpclient.cookies;

import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

public class TestPostCookies {
    private String url;
    private ResourceBundle bundle;
    private CookieStore cookieStore;

    @BeforeTest
    public void beforeTest(){
        this.bundle = ResourceBundle.getBundle("application", Locale.CHINA);
        this.url = this.bundle.getString("test.url");
    }

    @Test
    public void getCookies2() throws IOException {
        String cookiesUrl = this.url+this.bundle.getString("getcookies");
        HttpGet get = new HttpGet(cookiesUrl);

        this.cookieStore = new BasicCookieStore();
        CloseableHttpClient client = HttpClients.custom().setDefaultCookieStore(cookieStore).build();

        client.execute(get);

        List<Cookie> cookiesList = this.cookieStore.getCookies();
        for(Cookie cookie:cookiesList){
            String cookieName = cookie.getName();
            String cookieValue = cookie.getValue();
            System.out.println("cookies name:"+cookieName+"\n"+"cookies value:"+cookieValue);
        }
    }

    @Test(dependsOnMethods = "getCookies2")
    public void postUseCookies() throws JSONException, IOException {
        //拼接url
        String postUrl = this.url+this.bundle.getString("postcookies");

        //创建post请求方法
        HttpPost post = new HttpPost(postUrl);

        //添加post请求参数
        JSONObject param = new JSONObject();
        param.put("token","thistoken");
        param.put("id","110");

        //创建可携带cookies的client
        CloseableHttpClient client = HttpClients.custom().setDefaultCookieStore(this.cookieStore).build();

        //设置post请求头
        post.setHeader("content-type","application/json");

        //将参数添加到post请求体中
        StringEntity entity = new StringEntity(param.toString(),"utf-8");
        post.setEntity(entity);

        //发送post请求,创建响应对象，获取响应体
        HttpResponse response = client.execute(post);

        //处理响应结果，转化成json
        String result = EntityUtils.toString(response.getEntity(),"utf-8");
        System.out.println(result);
        JSONObject resultJson = new JSONObject(result);

        //获取响应结果
        String message = (String) resultJson.get("message");
        int code = (Integer) resultJson.get("code");

        //判断响应结果是否正确
        Assert.assertEquals("success!",message);
        Assert.assertEquals(200,code);

    }
}
