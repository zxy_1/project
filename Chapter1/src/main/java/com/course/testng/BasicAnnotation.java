package com.course.testng;

import org.testng.annotations.*;

public class BasicAnnotation {

    @Test
    public void test1(){
        System.out.println("test1   运行！");
    }

    @Test
    public void test2(){
        System.out.println("test2   运行！");
    }

    @Test(enabled = false)
    public void test3(){
        System.out.println("test3   运行！");
    }

    @Test(groups = "group1")
    public void test4(){
        System.out.println("group1 中的test4 运行！");
    }

    @Test(groups = "group1")
    public void test5(){
        System.out.println("group1 中的test5 运行！");
    }

    @Test(groups = "group2")
    public void test6(){
        System.out.println("group2 中的test6 运行！");
    }

    @BeforeMethod
    public void beforeMethod(){
        System.out.println("beforeMethod    运行！");
    }

    @AfterMethod
    public void afterMethod(){
        System.out.println("afterMethod    运行！");
    }

    @BeforeTest
    public void beforeTest(){
        System.out.println("beforeTest    运行！");
    }

    @AfterTest
    public void afterTest(){
        System.out.println("afterTest    运行！");
    }

    @BeforeClass
    public void beforeClass(){
        System.out.println("beforeClass    运行！");
    }

    @AfterClass
    public void afterClass(){
        System.out.println("afterClass     运行！");
    }

    @BeforeGroups(groups = "group1")
    public void beforeGroups(){
        System.out.println("beforeGroups 运行！");
    }

    @AfterGroups(groups = "group1")
    public void afterGroups(){
        System.out.println("afterGroups 运行！");
    }

    @BeforeSuite
    public void beforeSuite(){
        System.out.println("beforeSuite 运行！");
    }

    @AfterSuite
    public void afterSuite(){
        System.out.println("afterSuite 运行！");
    }

}
