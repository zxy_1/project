package com.course.testng;

import org.testng.annotations.Test;

public class ExcepectException {
    /*异常测试：
    * 预期结果就是抛出的异常
    * 异常测试
    * */
    @Test(expectedExceptions = RuntimeException.class)
    public void runTimeExceptionFaile(){
        System.out.println("异常测试!失败");
    }

    @Test(expectedExceptions = RuntimeException.class)
    public void runTimeExceptionSucess(){
        System.out.println("异常测试！成功");
        throw new RuntimeException();
    }
}
