package com.course.testng;

import org.testng.annotations.Test;
/*
* 依赖测试
* test2,依赖test1；test1失败test2忽略不执行
* */

public class DependTest {
    @Test
    public void test1(){
        System.out.println("test1 run!");
        throw new RuntimeException();
    }
    @Test(dependsOnMethods = {"test1"})
    public void test2(){
        System.out.println("test2 run!");
    }
}
