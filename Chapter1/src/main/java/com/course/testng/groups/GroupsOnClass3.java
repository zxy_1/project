package com.course.testng.groups;

import org.testng.annotations.Test;

@Test(groups = "teacher")
public class GroupsOnClass3 {
    public void teacher1(){
        System.out.println("GroupsOnClass3 中的 teacher11 运行！");
    }
    public void teacher2(){
        System.out.println("GroupsOnClass3 中的 teacher22 运行！");
    }
}
