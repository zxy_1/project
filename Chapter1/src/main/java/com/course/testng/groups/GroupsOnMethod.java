package com.course.testng.groups;

import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

/*
* 方法组
* */
public class GroupsOnMethod {
    @Test(groups = "server")
    public void test1(){
        System.out.println("server Groups test1 run!");
    }
    @Test(groups = "server")
    public void test2(){
        System.out.println("server Groups test2 run!");
    }
    @Test(groups = "client")
    public void test3(){
        System.out.println("client Groups test3 run!");
    }
    @Test(groups = "client")
    public void test4(){
        System.out.println("client Groups test4 run!");
    }
    @BeforeGroups("server")
    public void runGroups1(){
        System.out.println("runGroup1");
    }
    @AfterGroups("server")
    public void runGroups2(){
        System.out.println("runGroup2");
    }
}
