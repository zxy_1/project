beforeSuite 运行！                     测试套件最先执行
beforeTest    运行！                   测试执行前执行
beforeClass    运行！                  测试类执行前执行

beforeMethod    运行！                 测试方法执行前执行
test1   运行！                         测试方法执行
afterMethod    运行！                  测试方法执行后执行

beforeMethod    运行！
test2   运行！
afterMethod    运行！

beforeGroups 运行！                    测试组执行前执行
beforeMethod    运行！
group1 中的test4 运行！
afterMethod    运行！

beforeMethod    运行！
group1 中的test5 运行！
afterMethod    运行！
afterGroups 运行！

beforeMethod    运行！
group2 中的test6 运行！
afterMethod    运行！

afterClass     运行！
afterTest    运行！
afterSuite 运行！


testng中的注解：
@Test
@BeforeMethod
@AfterMethod
@BeforeClass
@AfterClass
@BeforeTest
@AfterTest
@BeforeSuite
@AfterSuite
@BeforeGroup
@AfterGroup

这里注解执行顺序：
@BeforeSuite
@BeforeTest
@BeforeClass
@BeforeMethod
@Test
@AfterMethod
@AfterClass
@AfterTest
@AfterSuite
